const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((req, res) => {
  try {
    let reqURLArr = req.url.split("/");
    
    if (reqURLArr.length > 3 || reqURLArr.length === 1) {
      throw new Error();
    }
    switch (reqURLArr[1]) {
      case "html":
        if( reqURLArr.length > 2 ) {
            throw new Error();
        }
        fs.readFile("./index.html", "utf-8", (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "application/json" });
            res.end(
              JSON.stringify({
                data: "HTML File not found",
              })
            );
          } else {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.end(data);
          }
        });

        break;

      case "json":
        if( reqURLArr.length > 2 ) {
            throw new Error();
        }
        fs.readFile("./data.json", "utf-8", (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "application/json" });
            res.end(
              JSON.stringify({
                data: "HTML File not found",
              })
            );
          } else {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(data);
          }
        });

        break;

      case "uuid":
        if( reqURLArr.length > 2 ) {
            throw new Error();
        }
        let data = { uuid: uuidv4() };

        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(JSON.stringify(data));
        break;

      case "status":
        let lastSegment = parseInt(reqURLArr[reqURLArr.length - 1]);
        let statusCode = http.STATUS_CODES;

        if (Object.keys(statusCode).includes(reqURLArr[reqURLArr.length - 1])) {
          res.writeHead(lastSegment, { "Content-Type": "application/json" });
          res.end(
            JSON.stringify({
              statusCodeMessage: statusCode[lastSegment],
              statusCode: lastSegment,
            })
          );
        } else {
          res.writeHead(400, { "Content-Type": "application/json" });
          res.end("Invalid status code");
        }

        break;

      case "delay":
        let delay = parseInt(reqURLArr[reqURLArr.length - 1]);
        
        if ( isNaN(delay) ) {
          throw new Error();
        } else {
          setTimeout(() => {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify(`Response after ${delay} seconds`));
          }, delay * 1000);
        }

        break;
        
      default:
        res.writeHead(400, { "Content-Type": "application/json" });
        res.end(
          JSON.stringify({
            data: "File not found",
          })
        );
    }
  } catch (error) {
    res.writeHead(400, { "Content-Type": "application/json" });
    res.end(
      JSON.stringify({
        data: "URL not found",
      })
    );
  }
});

const PORT = 1230;

server.listen(PORT, (err) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Listening to port: ", PORT);
  }
});
// console.log( server.address().port );
